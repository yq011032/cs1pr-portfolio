
#include "common.h"

static void move(void); 
static void touch(Entity* other);
static void calculateDist(Entity *e);
static void tick(void);

// X, Y, speed, range 
void initEnemy(char* line)
{
	Entity* e; // creating the pointer e 

	e = malloc(sizeof(Entity)); // creating space for e in the memory
	memset(e, 0, sizeof(Entity)); // setting e to the memory
	stage.entityTail->next = e;  
	stage.entityTail = e;

	sscanf(line, "%*s %f %f %f %f", &e->x, &e->y, &e->speed, &e->range); // reading the data in the formatted form (x, y, speed, range)

	calculateDist(e); // calculating the enemy distance

	e->health = 1; // defining health to e(enemy)

	e->moveForward = 1; // defining moveForward to e (enemy)
	e->move = move; // defining move to e 
	e->touch = touch; //defining touch to e
	e->tick = tick; //defining tick to e 

	e->texture = loadTexture("gfx/enemy.png"); // loading the enemy png image from gfx folder
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h); // assigning the enemy with w and h
	e->flags = EF_SOLID;
}

static void move(void) //  move function for enemy 
{
	self->count++;
	self->value += 0.1;
	self->y += sin(self->value);
}

static void tick(void) { //  tick function for enemy 
	
	if (self->moveForward == 1) { // if enemy moves forward
		self->x += self->speed;
		self->texture = loadTexture("gfx/enemy.png"); // assigning the enmey with enemy.png image from gfx folder
	} else if (self->moveBackwards == 1) { // if enemy moves backward
		self->x -= self->speed;
		self->texture = loadTexture("gfx/enemy reversed.png"); // assigning the enemey with enemy reversed.png image from gfx folder
	}

	if (self->x >= self->maxDist) { // if else statement
		self->moveForward = 0; // assigning value for enmey moving forward 
		self->moveBackwards = 1; // assigning value for enemy moving bakward
	} else if (self->x <= self->minDist) {
		self->moveForward = 1; //  assigning value for enmey moving forward
		self->moveBackwards = 0; //  assigning value for enemy moving bakward
	}

}

static void calculateDist(Entity* e) { // calculating distance for enemy 
	e->maxDist = e->x + e->range;  // assigning maximum distance to the enemy 
	e->minDist = e->x - e->range; // assigning minimum distance to the enmey 
}

static void touch(Entity* other) // touch function for the player 
{
	if (other == player)
	{
		int h = other->health - 1; // defining h as player health

		if (h >= 1 && self->count >= self->timer) {
			playSound(1, CH_PLAYER); // assigning sound to the player 
			other->health--;
			player->turnRed = 1; // assigning value to the player
			self->timer = self->count + 30;
		}
	}
}
