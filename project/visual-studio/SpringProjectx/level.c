void levelChange(int levelIndex) { // calling the function levelChange 
	switch (levelIndex) {
	case 1:
		initStage("data/map01.dat", "data/ents01.dat"); // adding map01 and ents01 to the game in case 1 (level 1)
		break;
	case 2:
		initStage("data/map02.dat", "data/ents02.dat"); // adding map02 and ents02 to the game in case 2 (level 2)
		break;
	case 3:
		initStage("data/map03.dat", "data/ents03.dat"); // adding map03 and ents03 to the game in case 3 (level 3)
		break;
	case 4:
		exit(1);
		break;
	}
}